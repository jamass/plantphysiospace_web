from django.contrib import admin
from .models import ExperimentData, ResultData, UserUploadCSVFile, UserUploadTXT, ReferenceData


class ExperimentDataAdmin(admin.ModelAdmin):
    pass


class ResultDataAdmin(admin.ModelAdmin):
    pass


class UserUploadCSVFileAdmin(admin.ModelAdmin):
    pass


class UserUploadTXTAdmin(admin.ModelAdmin):
    pass


class ReferenceDataAdmin(admin.ModelAdmin):
    pass

admin.site.register(ExperimentData, ExperimentDataAdmin)
admin.site.register(ResultData, ResultDataAdmin)
admin.site.register(UserUploadTXT, UserUploadTXTAdmin)
admin.site.register(UserUploadCSVFile, UserUploadCSVFileAdmin)
admin.site.register(ReferenceData, ReferenceDataAdmin)