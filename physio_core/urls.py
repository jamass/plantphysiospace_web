
from django.conf.urls import url
from django.conf import settings

from . import views
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^result$', views.result, name='result'),
    url(r'^result/(?P<result_id>[a-zA-Z0-9-]+)/$', views.detail, name='detail'),
    url(r'^resultjson/(?P<result_id>[a-zA-Z0-9-]+)/$', views.resultjson, name='resultjson'),
    url(r'^heatmap/(?P<result_id>[a-zA-Z0-9-]+)/$', views.heatmap, name='heatmap'),
    url(r'^tutorial/$', views.tutorial, name='tutorial'),
    url(r'^about$', views.about, name='about'),
    url(r'^error$', views.show_error_view, name='show_error_view'),
    url(r'^contact$', views.contact, name='contact'),
    url(r'^submit_csv$', views.submit_csv_colorchoice, name='submit_csv'),
    url(r'^submit_txt$', views.submit_txt, name='submit_txt'),
    url(r'^download_csv/(?P<result_id>[a-zA-Z0-9-]+)/$', views.csv_download_view, name = "download_csv"),
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'^account/reset_password_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', views.PasswordResetConfirmView.as_view(),name='reset_password_confirm'),
    url(r'^account/reset_password', views.ResetPasswordRequestView.as_view(), name="reset_password"),
    url(r'^account/login/$', 'django.contrib.auth.views.login', name='login'),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
if settings.DEBUG:
      urlpatterns.append(url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))

