# HOWTO work with physioProc.py
# Ali Hadizadeh Esfahani: esfahani@aices.rwth-aachen.de


##importing and initializing:
from physioProc import PhysioProcessor # you only need 'PhysioProcessor' class
PS = PhysioProcessor() # making an instance
PS.setPhysioRefAdrs(physioRefAdrs = "./ATH1.csv") # setting the reference, you can
# also set reference while constructing the object: PS = PhysioProcessor(physioRefAdrs= "./ATH1.csv")

##Data input: there are 2ways:
##1- Probe names as input:
PS.setData(uppedGenes=['AFFX-BioB-5_at', 'AFFX-BioB-M_at', 'AFFX-BioB-3_at','AFFX-BioC-5_at', 'AFFX-BioC-3_at'],
           downedGenes=['266463_at', '266464_at', '266465_at', '266466_at', '266467_at','266468_at']) # you can also set these at construction
print(PS.getResults()) # getting the scores

# Trying with soybean:
PS.setData(uppedGenes=["Gma.11323.1.S1_at","Gma.11323.1.S1_s_at","Gma.11323.1.S1_x_at","Gma.11323.2.S1_at","Gma.11324.1.A1_s_at",
                       "Gma.11324.2.S1_a_at","Gma.11324.2.S1_at","Gma.11325.1.S1_at","Gma.11326.1.S1_s_at","Gma.11326.2.S1_at",
                       "Gma.11328.1.S1_at","Gma.11330.1.S1_s_at","Gma.11330.2.S1_at","Gma.11330.3.S1_at","Gma.11333.1.S1_s_at",
                       "Gma.11333.2.S1_at","Gma.11334.1.S1_a_at","Gma.11336.1.S1_at","Gma.11336.2.S1_at","Gma.11337.1.S1_at",
                       "Gma.11338.1.S1_at","Gma.11339.1.S1_at","Gma.1134.1.S1_at","Gma.11341.1.S1_at"],
           downedGenes=["Gma.1617.1.S1_at","Gma.16170.1.S1_at","Gma.16171.1.S1_at","Gma.16171.1.S1_s_at","Gma.16172.1.S1_at",
                        "Gma.16174.1.S1_at","Gma.16175.1.A1_at","Gma.16176.1.S1_at","Gma.16177.1.A1_at","Gma.16181.1.S1_at",
                        "Gma.16189.1.S1_at","Gma.16189.2.S1_at"])
print(PS.getResults()) # getting the scores

##2- A matrix as input data: input should be a numpy array(rows as genes and columns as samples) with the
#  first row containing column names and first column row names.

# A CSV as input:
import csv
import numpy as np
inputMatFile = open("./KilianEtAlReduced.csv")
inputMatCsv = list(csv.reader(inputMatFile))
inputMat = np.array(inputMatCsv)

PS.setData(inpt = inputMat) # you can also set this at construction

## 'getResults' method has a 3member list as output: first is 'scores' with is the matrix of physioScores,
#  second is 'physioRefColNames' which are the row names for the heatmap, and last one is inputColNames which
#  are column names for the heatmap(inputColNames is None if input are gene names rather than a matrix)
[scores, physioRefColNames, inputColNames] = PS.getResults()
print(scores)

