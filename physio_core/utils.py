import time
import os
import random
from django.conf import settings


from .models import ResultData

def block(str):
    print ("Start : %s" % time.ctime())
    time.sleep(1)
    res = ResultData()
    res.data = str+" the result"
    res.id = str+"id"
    res.path = "blank"
    print(str)
    print("MEDIA",settings.MEDIA_ROOT)


    #with open(os.path.join(settings.MEDIA_ROOT,"tmp","{}.png".format(res.id)), "wb") as png_fil:
    #    png_fil.write(c.dump())
    res.save()
    print ("End : %s" % time.ctime())
    return res.id



def cluster(dataframe):
    import pandas as pd
    import scipy.spatial
    import scipy.cluster
    import numpy as np
    #import matplotlib.pyplot as plt
    # Example data: gene expression
    geneExp = {'genes' : ['a', 'b', 'c', 'd', 'e', 'f'],
     	   'exp1': [-2.2, 5.6, 0.9, -0.23, -3, 0.1],
	   'exp2': [5.4, -0.5, 2.33, 3.1, 4.1, -3.2]
          }
    #print(geneExp)
    df = pd.DataFrame( geneExp )
    dataMatrix = np.array( df[['exp1', 'exp2']] )
    print(dataMatrix, "datamatrix0")
    distMat = scipy.spatial.distance.pdist( dataMatrix )
    print(distMat)
    print(df, "dataframe")
    if isinstance(dataframe, pd.DataFrame):
        print(dataframe)
        print(list(dataframe.columns.values))
        print(np.array( df[['exp1', 'exp2']] ))
        df = dataframe
        dataMatrix = np.array( df[list(dataframe.columns.values)])
        print(dataMatrix, "datamatrix1")
        distMat = scipy.spatial.distance.pdist( dataMatrix )
    #df = dataframe
    # Determine distances (default is Euclidean)
    #dataMatrix = np.array( df[['exp1', 'exp2']] )
    #distMat = scipy.spatial.distance.pdist( dataMatrix )
    print(distMat)
    # Cluster hierarchicaly using scipy
    clusters = scipy.cluster.hierarchy.linkage(distMat, method='single')
    T = scipy.cluster.hierarchy.to_tree( clusters , rd=False )
    print(T)
    # Create dictionary for labeling nodes by their IDs
    #labels = list(df.genes)
    labels = list(df.index)
    id2name = dict(zip(range(len(labels)), labels))
    print(labels)
    # Draw dendrogram using matplotlib to scipy-dendrogram.pdf
    scipy.cluster.hierarchy.dendrogram(clusters, labels=labels, orientation='right',  no_plot=True  )
    #plt.savefig("scipy-dendrogram.png")

    # Create a nested dictionary from the ClusterNode's returned by SciPy
    def add_node(node, parent ):
        # First create the new node and append it to its parent's children
        newNode = dict( node_id=node.id, children=[] )
        parent["children"].append( newNode )

        # Recursively add the current node's children
        if node.left: add_node( node.left, newNode )
        if node.right: add_node( node.right, newNode )

    # Initialize nested dictionary for d3, then recursively iterate through tree
    d3Dendro = dict(children=[], name="Root1")
    add_node( T, d3Dendro )
    print(d3Dendro)
    # Label each node with the names of each leaf in its subtree
    def label_tree( n ):
        # If the node is a leaf, then we have its name
        if len(n["children"]) == 0:
                leafNames = [ id2name[n["node_id"]] ]
        
        # If not, flatten all the leaves in the node's subtree
        else:
                leafNames = reduce(lambda ls, c: ls + label_tree(c), n["children"], [])

        # Delete the node id since we don't need it anymore and
        # it makes for cleaner JSON
        del n["node_id"]

        # Labeling convention: "-"-separated leaf names
        n["name"] = name = "-".join(sorted(map(str, leafNames)))
        
        return leafNames

    label_tree( d3Dendro["children"][0] )


    # Output to JSON
    return(d3Dendro)

