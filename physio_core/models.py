from django.db import models
from django.db.models import signals

import datetime
import uuid
import pandas


def make_unique_directory_path(instance, filename):
    return 'documents/'+datetime.date.today().isoformat()+"/"+str(uuid.uuid4())+'/{0}'.format(filename)


def make_unique_directory_path_reference(instance, filename):
    return 'documents/reference/'+datetime.date.today().isoformat()+"/"+str(uuid.uuid4())+'/{0}'.format(filename)


class ModelWithTimestamp(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class UserUploadCSVFile(ModelWithTimestamp):
    def __unicode__(self):
        return str(self.csv)
    csv = models.FileField(upload_to=make_unique_directory_path)


class UserUploadTXT(ModelWithTimestamp):
    def __unicode__(self):
        return str(self.id)
    up = models.TextField(blank=False, null=False)
    down = models.TextField(blank=False, null=False)


class ReferenceData(ModelWithTimestamp):
    """ The data that is used to set up the physiospace """

    class Meta:
        verbose_name_plural = "reference data"

    def __unicode__(self):
        return self.name
    name = models.CharField(max_length=200, unique=True)
    description = models.TextField()
    reference_names = models.TextField(blank=True, null=True)
    csv = models.FileField(upload_to=make_unique_directory_path_reference)


def set_referencedata_reference_names(sender, instance, created, **kwargs):
    reftable = pandas.read_csv(instance.csv._get_path())
    ReferenceData.objects.filter(pk=instance.pk).update(reference_names=str(reftable.columns))
    ref_info =  [t.split(",") for t in reftable.columns]
    tmp  = "\n".join([",".join([u.strip() for u in t]) for t in ref_info])
    ReferenceData.objects.filter(pk=instance.pk).update(reference_names=str(tmp))

signals.post_save.connect(set_referencedata_reference_names, sender=ReferenceData)


class ExperimentData(ModelWithTimestamp):
    """ The data that is used to set up the physiospace """
    class Meta:
        verbose_name_plural = "experiment data"

    csv_filename = models.CharField(max_length=255)
    csv_text = models.TextField()

    # python 2 syntax. Use __str__ on python3.x
    def __unicode__(self):
        return self.csv_filename
    """ The data that will be mapped onto the physiospace """


class ResultData(ModelWithTimestamp):
    class Meta:
        verbose_name_plural = "result data"  # for the django admin only

    def __unicode__(self):
        return self.id

    id = models.CharField(max_length=255, primary_key=True)
    data = models.TextField(null=True, blank=True)
    divdata = models.TextField(null=True, blank=True)
    csvdata = models.TextField(null=True, blank=True)
    rawdata = models.TextField(null=True, blank=True)
