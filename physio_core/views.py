from django.shortcuts import get_object_or_404, render, redirect, render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.views.generic.edit import FormView
from django.contrib.auth.models import User
from django.db.models.query_utils import Q
from django.http import JsonResponse
#  imports for pw reset request
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template import loader
from django.core.validators import validate_email
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.core.mail import send_mail

from django.contrib import messages
from django.contrib.auth import get_user_model

from .forms import UserUploadCSVFileFormColorSelect, UserUploadTXTForm
from .models import UserUploadCSVFile, UserUploadTXT
from .models import ResultData, ReferenceData
from .forms import PasswordResetRequestForm, SetPasswordForm

from processor.physioProc import PhysioProcessor
import uuid
import datetime


# for sample output #######################################
from bokeh.charts import HeatMap
import bokeh.palettes
from bokeh.embed import components
# on embedding plots: http://bokeh.pydata.org/en/latest/docs/user_guide/embed.html
import pandas
import numpy as np
#############################################################
from django.conf import settings
from StringIO import StringIO

import logging
logging.basicConfig()
logger = logging.getLogger(__name__)

# todo: DEFAULT_FROM_EMAIL from settings
#DEFAULT_FROM_EMAIL = "exampleAdmin@example.com"

# d3
def heatmap(request, result_id):
    context =  {}
    template = 'physio_core/heatmap_d3.html'
    context['result'] = get_object_or_404(ResultData, pk=result_id)
    return render(request, template_name=template, context=context)

############
def resultjson(request, result_id):
    # rows: reference, source
    # columns: query, target
    result =  get_object_or_404(ResultData, pk=result_id)
    data = pandas.read_csv(StringIO(result.csvdata))
    resultjson = data.set_index('Unnamed: 0').to_dict(orient='split')
    res = {}
    res["links"] = []

    rownames = resultjson["index"]
    colnames = resultjson["columns"]

    def n(x,i):
        if len(x.split(","))>i:
            return x.split(",")[i].strip()
        else:
            return ""

    for i, v in enumerate(resultjson["data"]):
        for j, val in enumerate(v):
            res["links"].append({"source":i, "target": j, "value": val})
        res["nodes"] = [{
                        "name":x,
                        "group": n(x,4),
                        "accession": n(x,0),
                    }
                    for i,x in enumerate(rownames+colnames)
        ]

    res["queryNodes"] = [{
                             "name":x,
                             "accession": n(x,0),
                             "tissue": n(x,1),
                             "condition": n(x,2),
                             "group": n(x,4),
                             "AID": n(x,6),
                             "index": i,
                             "query":False
                         }
                         for i,x in enumerate(colnames)
                         ]

    res["referenceNodes"] = [{
                                 "name":x,
                                 "accession": n(x,0),
                                 "tissue": n(x,1),
                                 "condition": n(x,2),
                                 "group": n(x,4),
                                 "AID": n(x,6),
                                 "index": i,
                                 "query":True
                             }
                             for i,x in enumerate(rownames)
                             ]

    return JsonResponse(res, safe=False) #ok


# /d3
def about(request):
    context = {}
    return render(request, 'physio_core/about.html', context)
def tutorial(request):
    context = {}
    return render(request, 'physio_core/tutorial.html', context)

def contact(request):
    context = {}
    return render(request, 'physio_core/contact.html', context)


def index(request):
    result_list = ResultData.objects.all()  # all the results
    context = {'result_list': result_list}
    return render(request, 'physio_core/index.html', context)


def result(request):
    result_list = ResultData.objects.all()[:100]
    context = {'result_list': result_list}
    return render(request, 'physio_core/result.html', context)


def detail(request, result_id):
    result = get_object_or_404(ResultData, pk=result_id)
    return render(request, 'physio_core/detail.html', {'result': result})


def get_physioprocessor_result_from_csv(reference_csv, submission_csv):
    import csv
    PS = PhysioProcessor()
    PS.setPhysioRefAdrs(reference_csv)
    inputMatFile = open(submission_csv)
    inputMatCsv = list(csv.reader(inputMatFile))
    #  todo
    inputMat = np.array(inputMatCsv)
    PS.setData(inpt=inputMat)
    res = PS.getResults()
    #print(len(res[1]), len(res[2]))
    return res




def get_physioprocessor_result_from_lists(reference_csv, submission_up, submission_down):
    PS = PhysioProcessor()
    PS.setPhysioRefAdrs(reference_csv)
    #print(reference_csv)
    PS.setData(uppedGenes=submission_up, downedGenes=submission_down)
    res = PS.getResults()
    return res


def dequote(s):
    """
    From stackoverflow http://stackoverflow.com/questions/3085382/python-how-can-i-strip-first-and-last-double-quotes
    If a string has single or double quotes around it, remove them.
    Make sure the pair of quotes match.
    If a matching pair of quotes is not found, return the string unchanged.
    """
    if not s:
        return s
    if (s[0] == s[-1]) and s.startswith(("'", '"')):
        return s[1:-1]
    return s


def sanitize_txt(txt):
    txt = txt.split(",")
    txt = [t.strip() for t in txt]
    txt = [dequote(t).encode('utf-8') for t in txt]
    txt = ",".join(txt)
    return txt


def submit_txt(request):
    if request.method == "POST":
        postdata=request.POST
        pdd = dict(postdata.lists())
        form = UserUploadTXTForm(request.POST)
        if form.is_valid():  # todo refactor cleaning up
            newupload = UserUploadTXT()
            tmp_up = request.POST['up']
            newupload.up = sanitize_txt(tmp_up)
            tmp_down = request.POST['down']
            newupload.down = sanitize_txt(tmp_down)
            newupload.save()
            try:
                compendium_id= request.POST['compendium']
                reference = ReferenceData.objects.get(id=compendium_id).csv._get_path()
            except ObjectDoesNotExist as e:
                print(e)
                redirect('show_error_view')
            result_values, result_rownames, result_colnames = get_physioprocessor_result_from_lists(
                reference_csv=reference, submission_up=newupload.up.split(','),
                submission_down=newupload.down.split(',')
            )
            # todo visualize
            palette = get_colorpalette_from_string(request.POST['colorscheme'])

            if not result_colnames:
                result_colnames = ["data"]  # *30# len(newupload.up)

            df = pandas.DataFrame(result_values, index=result_rownames, columns=result_colnames)

            hm_title = ""
            try:
                hm = HeatMap(df, title=hm_title, palette=palette, height=1200, width=1200)

                script, div = components(hm)
                new_result_object = ResultData()
                new_result_object.id = str(uuid.uuid4())
                new_result_object.data = script
                new_result_object.divdata = div
                new_result_object.csvdata = str(df.to_csv())
                new_result_object.save()
                # if ResultData.objects.all().count() > 2:  # delete old entries
                    # startdate = datetime.date.today()- datetime.timedelta(days=2)
                    # enddate = datetime.date.today() - datetime.timedelta(days=1)
                    # print("delete{}".format(ResultData.objects.filter(created_on__lt=enddate)))
                    # ResultData.objects.filter(created_on__lt=enddate).delete()

                return redirect('heatmap/'+new_result_object.id+"/")
            except ValueError as e:
                logger.error(e)
                messages.error(request, 'Invalid Input')
                return  redirect('show_error_view')
    else:


        form = UserUploadTXTForm(initial={'up':
                                              """Gma.11323.1.S1_at, Gma.11323.1.S1_s_at, Gma.11323.1.S1_x_at, Gma.11323.2.S1_at, Gma.11324.1.A1_s_at, Gma.11324.2.S1_a_at, Gma.11324.2.S1_at, Gma.11325.1.S1_at, Gma.11326.1.S1_s_at, Gma.11326.2.S1_at, Gma.11328.1.S1_at, Gma.11330.1.S1_s_at, Gma.11330.2.S1_at, Gma.11330.3.S1_at, Gma.11333.1.S1_s_at, Gma.11333.2.S1_at, Gma.11334.1.S1_a_at, Gma.11336.1.S1_at, Gma.11336.2.S1_at, Gma.11337.1.S1_at, Gma.11338.1.S1_at, Gma.11339.1.S1_at, Gma.1134.1.S1_at, Gma.11341.1.S1_at'""",
                                          'down':
                                              """Gma.1617.1.S1_at, Gma.16170.1.S1_at, Gma.16171.1.S1_at, Gma.16171.1.S1_s_at, Gma.16172.1.S1_at, Gma.16174.1.S1_at, Gma.16175.1.A1_at, Gma.16176.1.S1_at, Gma.16177.1.A1_at, Gma.16181.1.S1_at, Gma.16189.1.S1_at, Gma.16189.2.S1_at"""
                                          }

                                 )

    return render_to_response('physio_core/submit_txt.html', {'form': form}, RequestContext(request))


def get_colorpalette_from_string(s):
    if s == 'yellowred':
        return bokeh.palettes.YlOrRd9
    elif s == 'yellowred_inv':
        return bokeh.palettes.YlOrRd9[::-1]

    elif s == 'yellowgreen':
        return bokeh.palettes.YlGn9
    elif s == 'yellowgreen_inv':
        return bokeh.palettes.YlGn9[::-1]

    elif s == 'whiteblack':
        return bokeh.palettes.Greys9
    elif s == 'whiteblack_inv':
        return bokeh.palettes.Greys9[::-1]

    else:
        return bokeh.palettes.YlOrRd9


def submit_csv_colorchoice(request):
    if request.method == "POST":
        form = UserUploadCSVFileFormColorSelect(request.POST, request.FILES)
        try:
            if form.is_valid():
                newupload = UserUploadCSVFile()
                 # todo display error on missing csv
                newupload.csv = request.FILES['csv']#form.cleaned_data['csv'] #request.FILES['csv']
                newupload.save()

                compendium_id= request.POST['compendium']
                reference = ReferenceData.objects.get(id=compendium_id).csv._get_path() #todo
                # now run the physioprocessor!
                try :
                    result_values, result_rownames, result_colnames = get_physioprocessor_result_from_csv(
                        reference_csv=reference, submission_csv=newupload.csv._get_path())
                except ValueError as e:
                    logger.error(e)
                    messages.error(request, 'Invalid csv')
                    return  redirect('show_error_view')

                palette = get_colorpalette_from_string(request.POST['colorscheme'])
                df = pandas.DataFrame(result_values, index=result_rownames, columns=result_colnames)

                hm = HeatMap(df, title="", palette=palette, height=1200, width=1200)
                script, div = components(hm)
                new_result_object = ResultData()
                new_result_object.id = str(uuid.uuid4())
                new_result_object.data = script
                new_result_object.divdata = div
                new_result_object.csvdata = str(df.to_csv())
                new_result_object.save()

                #if ResultData.objects.all().count() > 10:  # delete old entries
                #    enddate = datetime.date.today() - datetime.timedelta(days=1)
                #    logger.info("delete{}".format(ResultData.objects.filter(created_on__lt=enddate)))
                #    ResultData.objects.filter(created_on__lt=enddate).delete()
                return redirect('result/'+new_result_object.id+"/")  # redirect to .views.thanks
            else:
                render_to_response('physio_core/submit_csv.html', {'form': form}, RequestContext(request))
        except KeyError as e:
            logger.error(e)
    else:
        form = UserUploadCSVFileFormColorSelect()
    return render_to_response('physio_core/submit_csv.html', {'form': form}, RequestContext(request))


def csv_download_view(request, result_id):
    result = get_object_or_404(ResultData, pk=result_id)
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = "attachment; filename={}.csv".format(str(result_id))
    if result.csvdata:
        response.content = result.csvdata
    else:
        response.content = ""

    return response


def show_error_view(request, errormessage="Error"):

    context =  {}
    template = 'physio_core/errormsg.html'
    context['errormessage'] = errormessage
    return render(request, template_name=template, context=context)


# Password Reset
class ResetPasswordRequestView(FormView):
    template_name = "registration/reset_password.html"
    success_url = 'login'
    form_class = PasswordResetRequestForm

    @staticmethod
    def validate_email_address(email):
        '''
        Email address -> True, else -> False
        '''
        try:
            validate_email(email)
            return True
        except ValidationError:
            return False

    def post(self, request, *args, **kwargs):
        '''A normal post request which takes input from field "email_or_username" (in ResetPasswordRequestForm).
           Adapted from: http://ruddra.com/2015/09/18/implementation-of-forgot-reset-password-feature-in-django/
        '''
        form = self.form_class(request.POST)
        data = None
        if form.is_valid():
            data = form.cleaned_data["email_or_username"]
        if self.validate_email_address(data) is True:                 #uses the method written above
            '''
            If the input is an valid email address, then the following code will lookup for users associated with that email address. If found then an email will be sent to the address, else an error message will be printed on the screen.
            '''
            associated_users= User.objects.filter(Q(email=data)|Q(username=data))
            if associated_users.exists():
                for user in associated_users:
                        c = {
                            'email': user.email,
                            'domain': settings.PHYSIOSPACE_DOMAIN, #request.META['HTTP_HOST'],
                            'site_name': 'PhysioSpace',
                            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                            'user': user,
                            'token': default_token_generator.make_token(user),
                            'protocol': 'http',
                            }
                        subject_template_name='registration/password_reset_subject.txt'
                        # copied from django/contrib/admin/templates/registration/password_reset_subject.txt to templates directory
                        email_template_name='registration/password_reset_email.html'
                        # copied from django/contrib/admin/templates/registration/password_reset_email.html to templates directory
                        subject = loader.render_to_string(subject_template_name, c)
                        # Email subject *must not* contain newlines
                        subject = ''.join(subject.splitlines())
                        email = loader.render_to_string(email_template_name, c)
                        send_mail(subject, email, settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)
                result = self.form_valid(form)
                messages.success(request, 'An email has been sent to {}'.format(data))
                return result
            result = self.form_invalid(form)
            messages.error(request, 'No user is associated with this email address')
            return result
        else:
            '''
            If the input is an username, then the following code will
            lookup for users associated with that user.
            If found then an email will be sent to the user's address,
            else an error message will be printed on the screen.
            '''
            associated_users = User.objects.filter(username=data)
            if associated_users.exists():
                for user in associated_users:
                    c = {
                        'email': user.email,
                        'domain': settings.PHYSIOSPACE_DOMAIN, #request.META['HTTP_HOST'],
                        'site_name': 'PhysioSpace',
                        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                        'user': user,
                        'token': default_token_generator.make_token(user),
                        'protocol': 'http',
                        }
                    subject_template_name = 'registration/password_reset_subject.txt'
                    email_template_name = 'registration/password_reset_user.html'
                    subject = loader.render_to_string(subject_template_name, c)
                    # Email subject *must not* contain newlines
                    subject = ''.join(subject.splitlines())
                    email = loader.render_to_string(email_template_name, c)
                    send_mail(subject, email, settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)
                result = self.form_valid(form)
                messages.success(request, 'Instructions have been sent to {}'.format(data))
                return result
            result = self.form_invalid(form)
            messages.error(request, 'This username does not exist.')
            return result
        messages.error(request, 'Invalid Input')
        return self.form_invalid(form)


class PasswordResetConfirmView(FormView):
    template_name = "registration/reset_password.html"
    success_url = 'login'
    form_class = SetPasswordForm

    def post(self, request, uidb64=None, token=None, *arg, **kwargs):
        """
        View that checks the hash in a password reset link and presents a
        form for entering a new password.
        """
        UserModel = get_user_model()
        form = self.form_class(request.POST)
        assert uidb64 is not None and token is not None  # checked by URLconf
        try:
            uid = urlsafe_base64_decode(uidb64)
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            if form.is_valid():
                new_password = form.cleaned_data['new_password2']
                user.set_password(new_password)
                user.save()
                messages.success(request, 'Password has been reset.')
                return self.form_valid(form)
            else:
                messages.error(request, 'Password reset has not been unsuccessful.')
                return self.form_invalid(form)
        else:
            messages.error(request, 'The reset password link is no longer valid.')
            return self.form_invalid(form)



