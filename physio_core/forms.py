"""store form-related classes"""
from django.conf import settings
from django import forms
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
#import pandas

from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _
from .models import ExperimentData, UserUploadTXT, UserUploadCSVFile, ReferenceData
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
#from crispy_forms.bootstrap import TabHolder, Tab
from crispy_forms.layout import Submit, Fieldset #, MultiField, Div

import logging
logging.basicConfig()
logger = logging.getLogger(__name__)

#  Constants for colorscheme ###
PHYSIOSPACE_COLOR_CHOICES = [('yellowred', 'Yellow -> Red'),
                             ('yellowred_inv', 'Red -> Yellow'),
                             ('whiteblack', 'Greyscale'),
                             ('whiteblack_inv', 'Greyscale (inverted)'),
                             ('yellowgreen', 'Yellow -> Green'),
                             ('yellowgreen_inv', 'Green -> Yellow'),
                             ]
#################################


class CsvSubmissionForm(forms.ModelForm):

    class Meta:
        model = ExperimentData
        fields = ('csv_text',)
""" Create a ModelForm class for each model that shall be represented in a form """
""" Handling, saving data, flagging errors is done in views.py """




class UserUploadTXTForm(forms.ModelForm):
    choices = PHYSIOSPACE_COLOR_CHOICES
    compendium =  ReferenceData.objects.all()
    #print(compendium)
    try:
        default_compendium = ReferenceData.objects.get(name="ATH1.csv")
        reference_names = default_compendium.reference_names
    except ObjectDoesNotExist as e:
        logger.error(e)
        try:
            default_compendium = ReferenceData.objects.all()[0]
            reference_names = default_compendium.reference_names
        except ObjectDoesNotExist as e:
            logger.error(e)
            default_compendium = None
            reference_names = ""
    except MultipleObjectsReturned as e:
        try:
            default_compendium = ReferenceData.objects.all()[0]
            reference_names = default_compendium.reference_names
        except ObjectDoesNotExist as e:
            logger.error(e)
            default_compendium = None
            reference_names = ""

    #reference_names = ReferenceData.objects.get(name=default_compendium.name).reference_names #todo !!! save this on upload
    # no need to re-index every time!!

    # eg ['GSE5615', ' AtGen 1h', ' Flg22 1h vs C', ' 0', ' Hormone', ' TRUE', ' A0']
    #ref_info = [list("abcdefhsdfl")]*6
    # ref_info = [t.split(",") for t in reference_names.split("\n") if t]
    # index_by_id0 = [t[0] for t in ref_info] # 'GSE5615'
    # index_by_id1 = [t[1] for t in ref_info[1:]] # ' AtGen 1h'
    # index_by_id2 = [t[2] for t in ref_info[1:]] # ' Flg22 1h vs C'
    # index_by_id3 = [t[3] for t in ref_info[1:]] # ' 0'
    # index_by_id4 = [t[4] for t in ref_info[1:]] # ' Hormone'
    # index_by_id5 = [t[5] for t in ref_info[1:]] # ' TRUE'
    # index_by_id6 = [t[6] for t in ref_info[1:]] # ' A0'

    # s4 = sorted(set(index_by_id4))
    # s2 = sorted(set(index_by_id2))
    # s0 = sorted(set(index_by_id0))

    # uniq_by4 =  (zip([str(s.strip()) for s in s4], [str(s.strip()) for s in s4]))

    # uniq_by0 = (zip([str(s.strip()) for s in s0], [str(s.strip()) for s in s0]))

    # uniq_by2 = (zip([str(s.strip()) for s in s2], [str(s.strip()) for s in s2]))

    compendium = forms.ModelChoiceField(queryset=ReferenceData.objects.all().order_by('name'))
    colorscheme = forms.ChoiceField(required=True, initial=choices[0],
                                    widget=forms.Select, choices=choices)
    # categories = forms.MultipleChoiceField(required=False, initial=s4,
    #                                  choices=uniq_by4, widget=forms.CheckboxSelectMultiple)
    #
    # accessions = forms.MultipleChoiceField(required=False, initial=None,
    #                                  choices=uniq_by0)
    # descriptions = forms.MultipleChoiceField(required=False, initial=None,
    #                                  choices=uniq_by2)
    helper = FormHelper()
    helper.add_input(Submit('submit','Submit'))

    helper.layout = Layout(
         Fieldset(
             'Gene IDs',
             'up','down',
             'compendium',
             'colorscheme',
             # 'categories',
             # 'accessions',
             # 'descriptions'
         ),
    )

    class Meta:
        model = UserUploadTXT

        fields = ['up', 'down']
        fields += ['compendium']
        fields += ['colorscheme']
        # fields += ['categories']
        # fields += ['accessions']
        # fields += ['descriptions']







class UserUploadCSVFileFormColorSelect(forms.ModelForm):
    choices = PHYSIOSPACE_COLOR_CHOICES
    colorscheme = forms.ChoiceField(required=True, initial=choices[0],
                                    widget=forms.Select, choices=choices)
    compendium = forms.ModelChoiceField(queryset=ReferenceData.objects.all().order_by('name'))
    #compendium =  ReferenceData.objects.all()
    #print(compendium)
    try:
        default_compendium = ReferenceData.objects.get(name="ATH1.csv")
    except ObjectDoesNotExist as e:
        default_compendium = None
    except MultipleObjectsReturned as e:
        logger.error(e)

        default_compendium = ReferenceData.objects.all()[0]

    #reference_names = ReferenceData.objects.get(name=default_compendium.name).reference_names #todo !!! save this on upload



    #csv = forms.FileField(required=True)

    class Meta:
        model = UserUploadCSVFile
        fields = ['csv']
        fields_required =['csv', 'compendium']
        #fields +=['colorscheme']

    def clean(self):
         cleaned_data = super(UserUploadCSVFileFormColorSelect, self).clean()
         logger.debug(cleaned_data, "CLD", cleaned_data['csv'])
    # #
         content = cleaned_data['csv']
         #print(content,"CONTENT")
         #print(content.content_type, "content type", content._size)
         #print(content.content_type.split('/')[0], content.content_type)
         content_type = content.content_type.split('/')[0]
         if content_type in settings.CONTENT_TYPES:
             if content._size > settings.MAX_UPLOAD_SIZE:
                 raise forms.ValidationError(_('Please keep filesize under %s. Current filesize %s') %
                                             (filesizeformat(settings.MAX_UPLOAD_SIZE), filesizeformat(content._size)))
         else:
             raise forms.ValidationError('File type is not supported')
         return cleaned_data


class PasswordResetRequestForm(forms.Form):
    email_or_username = forms.CharField(label=("Email Or Username"), max_length=254)


class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': ("The two password fields didn't match."),
        }
    new_password1 = forms.CharField(label=("New password"),
                                    widget=forms.PasswordInput)
    new_password2 = forms.CharField(label=("New password confirmation"),
                                    widget=forms.PasswordInput)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                    )
        return password2
