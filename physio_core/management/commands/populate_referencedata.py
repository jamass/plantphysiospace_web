from django.core.files import File
from django.core.management.base import BaseCommand
from physio_core.models import ReferenceData
from os.path import basename

class Command(BaseCommand):
    help = 'Add compendium csv to the database'
    def add_arguments(self, parser):
        parser.add_argument('compendium_csv', type=str)
        parser.add_argument('description', type=str)
    def _create_reference(self, compendium_csv, description, name):
        new_compendium = ReferenceData()
        new_compendium.name = name
        f = open(compendium_csv,'r')
        djangofile = File(f)
        new_compendium.csv = djangofile
        new_compendium.description = description

        new_compendium.save()

        f.close()


    def handle(self, *args, **options):
        new_csv = options['compendium_csv']
        new_description = options['description']
        new_name = basename(new_csv)
        self._create_reference(compendium_csv=new_csv, description=new_description, name=new_name)