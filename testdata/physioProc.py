# First fry on writing a lite physioCalculator Object

# Ali Hadizadeh Esfahani: esfahani@aices.rwth-aachen.de

# imports:

import numpy as np
import scipy.stats as sps
import warnings
import csv

class physioThread(object):

    def __init__(self, physioRef = None, physioRefProbeNames = None, uppedGenes = None, downedGenes = None):
        self.__uppedGenes = uppedGenes
        self.__downedGenes = downedGenes
        self.__physioRef = physioRef
        self.__physioRefProbeNames = physioRefProbeNames
        self.__updateScores()

    def __updateScores(self):
        if ((self.__physioRef is not None) & (self.__uppedGenes is not None) &
                (self.__downedGenes is not None) & (self.__physioRefProbeNames is not None)):
            self.__calculateScores()
        else:
            self.__scores = None

    def __calculateScores(self):
        uppedIndx = self.__indxFinder(self.__physioRefProbeNames, self.__uppedGenes)
        downedIndx = self.__indxFinder(self.__physioRefProbeNames, self.__downedGenes)

        [uppedIndx,downedIndx] = self.__checkFoundIndx(uppedIndx=uppedIndx,downedIndx=downedIndx, assumedStatus="arabi")

        scores = [np.nan]*len(self.__physioRef[0])

        for colNum in range(len(self.__physioRef[0])):
            [stats, pValue] = sps.ranksums(self.__physioRef[uppedIndx, colNum], self.__physioRef[downedIndx, colNum])
            scores[colNum] = np.sign(stats) * pValue

        self.__scores = scores

    def __indxFinder(self,probeNames, wantedNames):
        # R match!

        if type(probeNames) is np.ndarray: probeNames = list(probeNames) # .index not working for ndarrays, have to
        if type(wantedNames) is np.ndarray: wantedNames = list(wantedNames) # clear this up later

        indx = []
        for name in wantedNames:
            if name in probeNames: indx.append(probeNames.index(name))  # duplications?!?!?!

        return indx

    def __checkFoundIndx(self,uppedIndx, downedIndx, assumedStatus, minNumGenes = 1):
        # Check to see if there is enough overlap between genes
        while ((len(uppedIndx) < minNumGenes) | (len(downedIndx) < minNumGenes)) & (assumedStatus != "break"):
            [uppedIndx,downedIndx,assumedStatus] = self.__convertGeneNames(assumedStatus=assumedStatus)
        return(uppedIndx,downedIndx)

    def __convertGeneNames(self,assumedStatus):
        # convert another plant probeIDs to ATH1:
        if assumedStatus == "arabi":
            cheatSheetFile = open("./UniqueAthal_in_Rice.csv")
            nextAssumeStatus = "rice"
        elif assumedStatus == "rice":
            cheatSheetFile = open("./UniqueAthal_in_SoyGene-1_0-st-v1.csv")
            nextAssumeStatus = "soy"
        elif assumedStatus == "soy":
            cheatSheetFile = open("./UniqueAthal_in_wheat.csv")
            nextAssumeStatus = "wheat"
        else:
            warnings.warn("Not enough overlap between test probe names and reference! returning NaNs...")
            nextAssumeStatus = "break"
            return([[],[],nextAssumeStatus])

        cheatSheetCsv = list(csv.reader(cheatSheetFile))
        cheatSheetMat = np.array(cheatSheetCsv)

        uppedGenes = cheatSheetMat[self.__indxFinder(cheatSheetMat[:,1], self.__uppedGenes),2]
        downedGenes = cheatSheetMat[self.__indxFinder(cheatSheetMat[:,1], self.__downedGenes),2]
        del cheatSheetCsv, cheatSheetMat
        uppedIndx = self.__indxFinder(self.__physioRefProbeNames, uppedGenes)
        downedIndx = self.__indxFinder(self.__physioRefProbeNames, downedGenes)
        return([uppedIndx,downedIndx,nextAssumeStatus])

    def setPhysioRefAndProbes(self,physioRef = None, physioRefProbeNames = None):
        self.__physioRef = physioRef
        self.__physioRefProbeNames = physioRefProbeNames
        self.__updateScores()

    def setUppedDownedGenes(self,uppedGenes = None, downedGenes = None):
        self.__uppedGenes = uppedGenes
        self.__downedGenes = downedGenes
        self.__updateScores()

    def getUppedDownedGenes(self):
        return([self.__uppedGenes, self.__downedGenes])

    def getScores(self):
        if self.__scores is None: warnings.warn("input not properly initialized yet! returning 'None'")
        return(self.__scores)


class PhysioProcessor(object):

    def __init__(self, physioRefAdrs = None, inpt = None, uppedGenes = None, downedGenes = None):
        self.__thread = physioThread()
        self.setData(inpt = inpt, uppedGenes = uppedGenes, downedGenes = downedGenes)
        self.__readPhysioRef(physioRefAdrs)
        self.__updateScores()

    def setData(self,inpt = None, uppedGenes = None, downedGenes = None):
        if (uppedGenes is not None) & (downedGenes is not None):
            self.__thread.setUppedDownedGenes(uppedGenes, downedGenes)
            self.__inpt = None
            self.__inptColNames = None
        elif (inpt is not None) & (uppedGenes is None) & (downedGenes is None):
            self.__thread.setUppedDownedGenes(uppedGenes, downedGenes)
            self.__calcUppedDownedGenes(inpt)
        else:
            self.__uppedGenes = None
            self.__downedGenes = None
            self.__inpt = None
        self.__updateScores()

    def __calcUppedDownedGenes(self,inpt):
        # I'm assuming 'inpt' is a numpy array with colnames in first row and row names in the first column.
        self.__inptColNames = inpt[0, 1:]
        self.__inptProbeNames = inpt[1:, 0]
        self.__inpt = inpt[1:, 1:].astype(np.double)

        nGenes = len(self.__inptProbeNames)
        nGeneLimit = int(round(0.05 * nGenes)) # Went with 5% for now
        nSamples = len(self.__inptColNames)

        self.__uppedGenesArray = np.array([[None] * nSamples] * nGeneLimit)
        self.__downedGenesArray = np.array([[None] * nSamples] * nGeneLimit)

        for colNum in range(nSamples):
            sortedIndx = self.__inpt[:,colNum].argsort()
            self.__uppedGenesArray[:,colNum] = [self.__inptProbeNames[i] for i in sortedIndx[-nGeneLimit:]]
            self.__downedGenesArray[:,colNum] = [self.__inptProbeNames[i] for i in sortedIndx[:nGeneLimit]]

    def __readPhysioRef(self,physioRefAdrs):

        if physioRefAdrs is None:
            self.__thread.setPhysioRefAndProbes(physioRef = None, physioRefProbeNames = None)
        else:

            physioFile = open(physioRefAdrs)
            physioList = list(csv.reader(physioFile))
            physioMat = np.array(physioList)

            self.__physioRefColNames = physioMat[0, 1:]
            self.__thread.setPhysioRefAndProbes(physioRef = physioMat[1:, 1:].astype(np.double), physioRefProbeNames = physioMat[1:, 0])

    def __updateScores(self):
        if None not in self.__thread.getUppedDownedGenes():
            self.__scores = self.__thread.getScores()
        elif self.__inpt is not None:
            nInpt = len(self.__inpt[0])
            nRef = len(self.__physioRefColNames)
            self.__scores = np.array([[None] * nInpt] * nRef)
            for colNum in range(nInpt):
                self.__thread.setUppedDownedGenes(uppedGenes = self.__uppedGenesArray[:,colNum],
                                                  downedGenes = self.__downedGenesArray[:,colNum])
                self.__scores[:,colNum] = self.__thread.getScores()

    def setPhysioRefAdrs(self, physioRefAdrs):
        self.__readPhysioRef(physioRefAdrs)
        self.__updateScores()

    def getResults(self):
        return([self.__scores, self.__physioRefColNames, self.__inptColNames])