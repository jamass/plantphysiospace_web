# Physiospace Web


## Setup
- Create virtual environment (for python 2.7) and activate:
```
virtualenv env
source env/bin/activate
```
- Update pip (optional):
```
pip install --upgrade pip
```
- Install from requirements.txt (this will take a while):
``` 
pip install -r requirements.txt
```

(In your virtualenv) Run locally with
```
python manage.py migrate
python manage.py runserver 8888
```

Visit the site with your browser at
`` http://localhost:8888 ``

To create an admin account, run 
``
python manage.py createsuperuser
``
and create a superuser.

Then visit  ``http://localhost:8888/admin ``

Here, you can add the ReferenceData objects (right now, it needs one object with the "name" set to "ATH1.csv", like the filename in the testdata directory). 

Once the ReferenceData is there, you can submit csv files from the web page ( http://localhost:8888/submit_csv ).
Right now, for testing purposes, all the submission results are stored and shown on the index page.

Instead of the admin interface, you can also use the management command
```python manage.py populate_referencedata testdata/ATH1.csv "ATH1.csv"```
to populate the db with compendium data

## Troubleshooting
If you get any weird error messages, that say sth about something not working with the database (missing columns for a model etc), the easiest way to solve it is to delete the database file 
`` rm db.sqlite3 ``
and remove the migrations folder
`` rm -r physio_core/migrations/ `` 
After that you will have to migrate, create a new superuser, and upload the reference dataset again (like above).


## Notes on dependencies
Most dependencies come from bokeh (and are not actually used, like the bokeh server w/ flask and jinja2 etc). Bokeh will only be used to generate html which will be handed over to the template. We might want to see if that can be replaced afterwards with plain d3.js, but for getting things going, it's a nice and easy way to produce visualizations.